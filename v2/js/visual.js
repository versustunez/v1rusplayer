let TWO_PI = 2 * Math.PI;
let selectVisual1 = () => {
    return {
        lastVector: null,

        draw: (style, render, vtf) => {
            let c = config.visual;
            style.fillColor(0, 0, 0, 0).background();
            let {dataArray, n} = getData();
            let steps = TWO_PI / c.steps;
            let color = c.color;
            if (c.staticColor) {
                style.fillColor(color.r, color.g, color.b, color.a);
            } else {
                style.fillColor(n * color.r, n * color.g, n * color.b, color.a);
            }
            render.translate(frame.getWidth() / 2, frame.getHeight() / 2).rotate(-95);
            visual.circle(render, 0, dataArray, steps);
            if (c.innerRing && c.radius > 10) {
                style.fillColor(0, 0, 0, 0);
                render.circle(0, 0, c.radius - 5);
            }
            render.resetTranslate();
        },

        circle: (render, counter, data, steps) => {
            if (data === undefined) {
                return;
            }
            render.startVertex(1);
            for (let i = 0; i < TWO_PI; i += steps) {
                let shift = VTUtils.normalize(data[counter], 255, 0) * config.visual.spikes;
                let x, y;
                y = (config.visual.radius + shift) * Math.sin(i);
                x = (config.visual.radius + shift) * Math.cos(i);
                counter++;
                render.vertex(new VTVector(x, y));
            }
            render.stopVertex()
        }
    };
};
let selectVisual2 = () => {
    return {
        draw: (style, render, vtf) => {
            let c = config.visual;
            let {dataArray, n} = getData();
            style.fillColor(0, 0, 0, 0).background();
            let color = c.color;
            if (c.staticColor || n > .95) {
                style.fillColor(color.r, color.g, color.b, color.a);
            } else {
                style.fillColor(n * color.r, n * color.g, n * color.b, color.a);
            }
            let bar = Math.floor(vtf.getWidth() / (config.visual.steps));
            let x = 0;
            render.translate(0, frame.getHeight());
            render.startVertex(1);
            for (let data of dataArray) {
                let height = VTUtils.normalize(data, 255, 0) * Math.floor(vtf.getHeight() / 4 * 3);
                render.vertex(new VTVector(x, -height));
                x += bar;
            }
            render.vertex(new VTVector(vtf.getWidth(), vtf.getHeight()));
            render.vertex(new VTVector(0, vtf.getHeight()));
            render.stopVertex();
            render.resetTranslate();
        },
    };
};


let getData = (items) => {
    items = items || config.visual.steps * 2;
    let dataArray = new Uint8Array(items);
    config.audio.analyser.getByteFrequencyData(dataArray);
    let sum = 0;
    for (let item of dataArray) {
        sum += item;
    }
    let n = VTUtils.normalize(sum, 255 * items, 0);
    return {dataArray, n};
};

let visual = selectVisual2();