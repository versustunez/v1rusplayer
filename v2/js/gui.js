document.body.addDelegatedEventListener('click', '.btn', (event, target) => {
    let toToggle = target.dataset.toToggle;
    if (toToggle) {
        let toggle = document.body.querySelector('[data-toggle="' + toToggle + '"]');
        if (toggle) {
            toggle.classList.toggle('active');
        }
        return;
    }
    let toCall = target.dataset.toCall;
    if (toCall) {
        if (config.ground.callAble[toCall]) {
            config.ground.callAble[toCall](target);
        }
    }
});
let updateFunc = (event, target) => {
    let {id, from} = target.dataset;
    if (id === 'color') {
        config[from][id][target.dataset.color] = parseFloat(target.value);
    } else {
        config[from][id] = parseFloat(target.value);
    }

    if (target.getAttribute('type') === 'checkbox') {
        config[from][id] = target.checked;
    }
    if (config.ground.callAble['update' + id]) {
        config.ground.callAble['update' + id]();
    }
    saveConfig();
};
let queueInner = document.querySelector('.playlist-wrap');

function queueSongs(event, target) {
    queue = [];
    let queueList = document.createElement('ul');
    queueList.classList.add('list');
    queueList.classList.add('playlist');
    for (let file of target.files) {
        if (file && file.type.indexOf('audio') !== -1 && file.name.match(".m3u") === null) {
            queue.push(file);
            let item = document.createElement('li');
            item.dataset.id = "" + queue.length - 1;
            item.innerText = file.name;
            queueList.appendChild(item);
        }
    }
    queueInner.innerHTML = "";
    queueInner.appendChild(queueList);
    song.current = 0;
    song.next = 0;
    nextSong();
}

function changePlaylist(index) {
    let active = queueInner.querySelector('.active');
    if (active) {
        active.classList.remove('active');
    }
    let now = queueInner.querySelector('[data-id="' + index + '"]');
    if (now) {
        now.classList.add('active');
        now.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
    }
}

document.body.addDelegatedEventListener('input', 'input[type="range"]', updateFunc);
document.body.addDelegatedEventListener('change', 'input[type="checkbox"]', updateFunc);
document.body.addDelegatedEventListener('change', 'input[type="file"]', queueSongs);
document.body.addDelegatedEventListener('input', 'input[type="search"]', (event, target) => {
    if (target.value.length < 4 && target.value.length !== 0) {
        return
    }
    let searchString = target.value.toLowerCase();
    let items = queueInner.querySelector('ul').children;
    for (let item of items) {
        let id = item.dataset.id;
        if (!queue[id].name.toLowerCase().includes(searchString)) {
            item.classList.add('hide');
        } else {
            item.classList.remove('hide');
        }
    }
});

document.body.addDelegatedEventListener('click', '.playlist-wrap li', (event, target) => {
    let index = target.dataset.id;
    song.current = parseInt(index);
    loadSong();
});