class VTNoise {
    constructor(seed) {
        seed = seed || Math.random();
        this.seed = VTUtils.random(-seed, seed);

        this.lastValue = 0;
    }

    static noise(value, seed) {
        seed = seed || Math.random();
        seed = VTUtils.random(-seed, seed);
        return value + seed;
    }

    logicNoise(value) {
        let randomValue;
        if (this.lastValue < 0) {
            randomValue = VTUtils.random(this.seed);
        } else {
            randomValue = VTUtils.random(-this.seed, 0);
        }

        this.lastValue = randomValue;
        return value + randomValue;
    }

}

class VTUtils {
    static random(min, max) {
        let rand = Math.random();
        if (typeof min === 'undefined') {
            return rand;
        } else if (typeof max === 'undefined') {
            if (min instanceof Array) {
                return min[Math.floor(rand * min.length)];
            } else {
                return rand * min;
            }
        } else {
            if (min > max) {
                let tmp = min;
                min = max;
                max = tmp;
            }
            return rand * (max - min) + min;
        }
    };

    static randomInt(min, max) {
        return Math.floor(VTUtils.random(min, max));
    }

    static normalize(val, max, min) {
        return (val - min) / (max - min);
    };

    static getColor(a, b, c, alpha, mode, useAlpha) {
        if (mode === 'hex') {
            a = a || '#FFF';
            return a;
        }
        if (a && b === undefined) {
            return a;
        }
        a = a || '0';
        b = b || '0';
        c = c || '0';
        alpha = alpha || '1';

        let color = a + ',' + b + ',' + c;
        if (useAlpha) {
            color += ',' + alpha;
        }
        return `${mode}(${color})`;
    }

    static distance(x, y, x2, y2) {
        let a = x - x2;
        let b = y - y2;

        return Math.sqrt(a * a + b * b);
    }

    static map(n, start1, stop1, start2, stop2, withinBounds) {
        let newVal = (n - start1) / (stop1 - start1) * (stop2 - start2) + start2;
        if (!withinBounds) {
            return newVal;
        }
        if (start2 < stop2) {
            return this.constrain(newVal, start2, stop2);
        } else {
            return this.constrain(newVal, stop2, start2);
        }
    };

    static constrain(n, low, high) {
        return Math.max(Math.min(n, high), low);
    }
}

class VTVector {
    constructor(x, y, z) {
        this.x = x || 0;
        this.y = y || 0;
        this.z = z || 0;
    }

    //helper
    static createRandom(x, y, z) {
        x = x || 1;
        y = y || 1;
        z = z || 0;
        return new VTVector(VTUtils.random(-x, x), VTUtils.random(-y, y), VTUtils.random(-z, z));
    }

    mult(times) {
        this.x *= times;
        this.y *= times;
        this.z *= times;
    }

    set(vector) {
        this.x = vector.x;
        this.y = vector.y;
        this.z = vector.z;
    }

    add(vector) {
        this.x = this.x + vector.x;
        this.y = this.y + vector.y;
        this.z = this.z + vector.z;
    }

    addXYZ(x, y, z) {
        this.x = x || 0;
        this.y = y || 0;
        this.z = z || 0;
        this.x = this.x + x;
        this.y = this.y + y;
        this.z = this.z + z;
    }

    clone() {
        return new VTVector(this.x, this.y, this.z);
    }
}

class VTEngine {
    constructor(frame, config) {
        this.frame = frame;
        this.config = config;
        this.blLoop = true;
        return this;
    }

    initListener() {
        document.addEventListener('keypress', this.config.keyPressed.bind(this));
        document.addEventListener('keydown', this.config.keyDown.bind(this));
        document.addEventListener('keyup', this.config.keyUp.bind(this));
        document.addEventListener('mousemove', this.config.mouseMove.bind(this));
        document.addEventListener('mousedown', this.config.mouseDown.bind(this));
        document.addEventListener('mouseup', this.config.mouseUp.bind(this));
    }

    start() {
        this.initListener();
        this.config.setup.call(this.frame);
        this.loop();
        console.timeEnd("VTFrame_" + this.frame.id);
    }

    loop() {
        if (this.blLoop) {
            requestAnimationFrame(this.loop.bind(this));
        }
        this.config.draw.call(this.frame);
    }

    loopMode(boolean) {
        this.blLoop = boolean;
        if (boolean) {
            this.loop();
        }
    }

    fps() {
        if (!this.lastRun) {
            this.lastRun = new Date().getTime();
            return 0;
        }
        let delta = (new Date().getTime() - this.lastRun) / 1000;
        this.lastRun = new Date().getTime();
        return Math.floor(1 / delta);
    }
}

window._vtfc = 0;

class VTFrame {
    constructor(config) {
        this.id = window._vtfc++;
        console.time("VTFrame_" + this.id);
        config = VTFrame.getDefaultConfig(config);
        this.config = config;
        this.canvas = VTFrame.createCanvas(config.width, config.height, config);
        this.style = new VTStyle(this.canvas, config);
        this.renderer = new VTRenderer(this.style, config);
        this.engine = new VTEngine(this, config);
        this.eventSet = false;
        return this;
    }

    static createCanvas(width, height, config) {
        width = width || 800;
        height = height || 600;
        let c = document.createElement('canvas');
        c.height = height;
        c.width = width;
        if (!config['appendTo'] instanceof Node) {
            config['appendTo'] = document.body;
        }
        config['appendTo'].appendChild(c);
        return c;
    }

    static getDefaultConfig(config) {
        this.getDefault('mode', '2d', config);
        this.getDefault('height', innerHeight, config);
        this.getDefault('width', innerWidth, config);
        this.getDefault('appendTo', document.body, config);
        this.getDefault('draw', 'f', config);
        this.getDefault('setup', 'f', config);
        this.getDefault('preload', 'f', config);
        this.getDefault('keyPressed', 'f', config);
        this.getDefault('keyDown', 'f', config);
        this.getDefault('keyUp', 'f', config);
        this.getDefault('mouseDown', 'f', config);
        this.getDefault('mouseUp', 'f', config);
        this.getDefault('mouseMove', 'f', config);
        return config;
    }

    static getDefault(parameter, callback, config) {
        if (callback === 'f') {
            callback = function () {
            };
        }
        config[parameter] = config[parameter] || callback;
    }

    run() {
        this.engine.start.call(this.engine);
    }

    getHeight() {
        return this.canvas.height;
    }

    getWidth() {
        return this.canvas.width;
    }

    resize(width, height) {
        this.canvas.width = width;
        this.canvas.height = height;
    }

}

let VTLOADER_EVENT = 'VTLoader_Preload';

class VTRenderer {
    constructor(VTCanvas) {
        this.canvas = VTCanvas;
    }

    start() {
        let context = this.canvas.context;
        context.beginPath();
        return context;
    }

    end(context) {
        context.closePath();
        this.canvas.draw();
    }

    ellipse(x, y, r1, r2) {
        let context = this.start();
        context.ellipse(x, y, r1, r2, 0, 0, 2 * Math.PI);
        this.end(context);
        return this;
    }

    circle(x, y, d) {
        let context = this.start();
        context.arc(x, y, d, 0, 2 * Math.PI);
        this.end(context);
        return this;
    }

    point(x, y, d) {
        let context = this.start();
        context.arc(x, y, d, 0, 2 * Math.PI);
        context.fill();
        context.closePath();
        return this;
    }

    rect(x, y, w, h) {
        let context = this.start();
        context.rect(x, y, w, h);
        this.end(context);
        return this;
    }

    fromTo(x1, y1, x2, y2) {
        let context = this.start();
        context.moveTo(x1, y1);
        context.lineTo(x2, y2);
        this.end(context);
        return this;
    }

    fromToCurved(x1, y1, x2, y2) {
        let context = this.start();
        context.moveTo(x1, y1);
        this.fromToCurvedNCTX(x1, y1, x2, y2, context);
        this.end(context);
        return this;
    }

    fromToCurvedNCTX(x1, y1, x2, y2, context) {
        let x_mid = (x1 + x2) / 2;
        let y_mid = (y1 + y2) / 2;
        let cp_x1 = (x_mid + x1) / 2;
        let cp_x2 = (x_mid + x2) / 2;
        context.quadraticCurveTo(cp_x1, y1, x_mid, y_mid);
        context.quadraticCurveTo(cp_x2, y2, x2, y2);
    }

    image(image, x, y, w, h) {
        if (!image) {
            return;
        }
        w = w || image.width;
        h = h || image.height;
        x = x || 0;
        y = y || 0;
        let context = this.start();
        context.drawImage(image, x, y, w, h);
        this.end(context);
        return this;
    }

    imageSprite(frame, posX, posY, sizeW, sizeH) {
        if (!sprite instanceof VTSprite) {
            return;
        }
        sizeW = sizeW || sprite.w;
        sizeH = sizeH || sprite.h;
        posX = posX || 0;
        posY = posY || 0;
        let context = this.start();
        context.drawImage(frame.image, frame.item.x, frame.item.y, frame.w, frame.h, posX, posY, sizeW, sizeH);
        this.end(context);
        return this;
    }

    translate(x, y) {
        x = x || 0;
        y = y || 0;
        let context = this.start();
        context.translate(x, y);
        return this;
    }

    resetTranslate() {
        let context = this.start();
        context.setTransform(1, 0, 0, 1, 0, 0);
        this.end(context);
        return this;
    }

    setFont(size, fontFamily) {
        size = size || 16;
        fontFamily = fontFamily || 'Arial';
        let context = this.canvas.context;
        context.font = size + 'px' + ' ' + fontFamily;
        return this;
    }

    //text utils
    text(string, x, y) {
        string = string || '';
        x = x || 0;
        y = y || 0;

        let context = this.start();
        context.fillText(string, x, y);
        return this;
    }

    save() {
        this.canvas.context.save();
        return this;
    }

    restore() {
        this.canvas.context.restore();
        return this;
    }

    startVertex(t) {
        this.vertexPoints = [];
        this.vertexTension = t || 1;
    }
    vertex(vector) {
        if (!this.vertexPoints) {
            this.vertexPoints = [];
        }
        this.vertexPoints.push(vector);
    }

    rotate(angle) {
        let ctx = this.start();
        ctx.rotate(angle * Math.PI / 180);
        this.end(ctx);
        return this;
    }

    stopVertex() {
        let points = this.vertexPoints || [];
        if (points.length === 0) {
            return;
        }
        let ctx = this.canvas.context;
        ctx.moveTo(points[0].x, points[0].y);

        let t = this.vertexTension || 1;
        for (let i = 0; i < points.length - 1; i++) {
            let p0 = (i > 0) ? points[i - 1] : points[0];
            let p1 = points[i];
            let p2 = points[i + 1];
            let p3 = (i !== points.length - 2) ? points[i + 2] : p2;

            let cp1x = p1.x + (p2.x - p0.x) / 6 * t;
            let cp1y = p1.y + (p2.y - p0.y) / 6 * t;

            let cp2x = p2.x - (p3.x - p1.x) / 6 * t;
            let cp2y = p2.y - (p3.y - p1.y) / 6 * t;

            ctx.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, p2.x, p2.y);
        }
        this.end(ctx);
        this.vertexPoints = null;
        this.vertexTension = null;
    }
}

class VTStyle {
    constructor(canvas, config) {
        this.canvas = canvas;
        this.context = canvas.getContext(config.mode);
        this.init();
    }

    init() {
        this.config = {
            stroke: true,
            fill: false,
            colorMode: 'rgb',
            alpha: false
        }
    }

    colorMode(mode) {
        mode = mode || 'rgb';
        this.config.colorMode = mode;
        return this;
    }

    fillColor(a, b, c, d) {
        let context = this.context;
        let mode = this.config.colorMode;
        context.fillStyle = VTUtils.getColor(a, b, c, d, mode, this.config.alpha);
        return this;
    }

    strokeColor(a, b, c, d) {
        let context = this.context;
        let mode = this.config.colorMode;
        context.strokeStyle = VTUtils.getColor(a, b, c, d, mode, this.config.alpha);
        return this;
    }

    alpha(boolean) {
        this.config.alpha = boolean;
        return this;
    }

    strokeWeight(weight) {
        this.context.lineWidth = weight;
        return this;
    }

    stroke(boolean) {
        this.config.stroke = boolean;
        return this;
    }

    fill(boolean) {
        this.config.fill = boolean;
        return this;
    }

    draw() {
        let context = this.context;
        let config = this.config;
        if (config.stroke)
            context.stroke();
        if (config.fill)
            context.fill();
        return this;
    }

    background() {
        let context = this.context;
        context.beginPath();
        context.fillRect(0, 0, this.canvas.width, this.canvas.height);
        context.closePath();
        return this;
    }

    clear() {
        let context = this.context;
        context.beginPath();
        context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        context.closePath();
        return this;
    }

    getContext() {
        return this.context;
    }
}