let vtFrameConfig = {
    mode: '2d',
    height: config.ground.visualContainer.clientHeight,
    width: config.ground.visualContainer.clientWidth,
    appendTo: config.ground.visualContainer,
    setup: function () {
        createAudioContext(config.audio.file);
        this.style.alpha(true).stroke(true).fill(true);
        this.style.getContext().lineJoin = 'miter';
    },
    draw: function () {
        let style = frame.style,
            render = frame.renderer;
        if (config.audio.file.src !== "" && !config.audio.file.paused) {
            visual.draw(style, render, this);
        }
    },
    keyPressed: function (event) {
        if (event.key === ' ') {
            if (document.querySelector('input[type="search"]') === document.activeElement) {
                return;
            }
            togglePlay();
        }
    }
};
window.addEventListener('resize', function () {
    frame.resize(config.ground.visualContainer.clientWidth, config.ground.visualContainer.clientHeight);
});
let frame = new VTFrame(vtFrameConfig);
frame.run();


//generate Callables
let callAble = config.ground.callAble;

callAble['updatesmoothingTimeConstant'] = () => {
    if (config.audio.analyser != null) {
        config.audio.analyser.smoothingTimeConstant = config.audio.smoothingTimeConstant;
    }
};

callAble['updatevolume'] = () => {
    config.audio.file.volume = VTUtils.normalize(config.audio.volume, 100, 0);
};

callAble['next'] = () => {
    nextSong();
};
callAble['prev'] = () => {
    prevSong();
};
callAble['shuffle'] = (target) => {
    config.audio.shuffle = !target.classList.contains('active');
    if(config.audio.shuffle) {
        target.classList.add('active');
    } else {
        target.classList.remove('active');
    }
};
callAble['toggleAudio'] = () => {
    togglePlay();
};

callAble['clearPlaylist'] = () => {
    queue = [];
    song.current = 0;
    song.next = 0;
    queueInner.innerHTML = "";
};