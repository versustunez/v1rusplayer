let config = {
    visual: {
        color: {
            r: 255,
            g: 0,
            b: 137,
            a: 1
        },
        staticColor: false,
        spikes: 200,
        steps: 512,
        radius: 100,
        innerRing: true,
    },
    audio: {
        volume: 100,
        smoothingTimeConstant: .6,
        shuffle: false,
        preload: true,
        file: new Audio(),
        analyser: null,
        audioCtx: null
    },
    ground: {
        visualContainer: document.querySelector('#visual'),
        callAble: {}
    }
};

function saveConfig() {
    localStorage.removeItem('config');
    localStorage.setItem('config', JSON.stringify(config));
}

function loadConfig() {
    let item = localStorage.getItem('config');
    if (item && item !== "") {
        config = JSON.parse(item);
        config.audio.file = new Audio();
        config.ground.visualContainer = document.querySelector('#visual');
    }
}

loadConfig();