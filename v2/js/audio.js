function createAudioContext(audio) {
    let AudioContext = window.AudioContext || window.webkitAudioContext;
    let ACTX = new AudioContext();
    let analyser = ACTX.createAnalyser();
    let MEDIA_ELEMENT_NODES = new WeakMap();
    let Source;

    analyser.fftSize = 4096;
    analyser.maxDecibels = 0;
    analyser.smoothingTimeConstant = .6;
    if (MEDIA_ELEMENT_NODES.has(audio)) {
        Source = MEDIA_ELEMENT_NODES.get(audio);
    } else {
        Source = ACTX.createMediaElementSource(audio);
        MEDIA_ELEMENT_NODES.set(audio, Source);
    }

    Source.connect(analyser);
    analyser.connect(ACTX.destination);
    analyser.smoothingTimeConstant = config.audio.smoothingTimeConstant;
    analyser.fftSize = 4096;
    analyser.frequencyBinCount = 4096;
    config.audio.audioCtx = ACTX;
    config.audio.analyser = analyser;
    audio.oncanplay = () => {
        togglePlay();
    };
    audio.onended = function () {
        nextSong();
    };
    audio.ontimeupdate = function () {
        document.body.style.setProperty('--progress-width', audio.currentTime / audio.duration * 100 + '%');
    }
}

let pausePlayIcons = document.querySelector('.pause-play').children;

// audio handler
function togglePlay() {
    if (config.audio.file.src === '') {
        return;
    }
    if (config.audio.audioCtx.state === 'suspended') {
        config.audio.audioCtx.resume();
    }

    if (config.audio.file.paused) {
        config.audio.file.play();
        pausePlayIcons[0].classList.remove('hide');
        pausePlayIcons[1].classList.add('hide');
    } else {
        config.audio.file.pause();
        pausePlayIcons[0].classList.add('hide');
        pausePlayIcons[1].classList.remove('hide');
    }
}

let song = {
    current: 0,
    next: 0,
};

let queue = [];

function nextSong() {
    if (config.audio.shuffle) {
        song.current = song.next;
        song.next = VTUtils.randomInt(0, queue.length - 1);
    } else {
        if (song.current === queue.length - 1) {
            song.current = 0;
            song.next = 1;
        } else {
            song.current = song.next;
            if (song.next + 1 === queue.length - 1) {
                song.next = 0;
            } else {
                song.next += 1;
            }
        }
    }

    loadSong();
}

function prevSong() {
    if (config.audio.shuffle) {
        return;
    }
    if (song.current === 0) {
        song.current = queue.length - 1;
    } else {
        song.current -= 1;
    }
    song.next = song.current + 1;

    loadSong();
}

let lastSong;

function loadSong() {
    if (queue.length === 0) {
        return;
    }
    let index = song.current;
    let file = queue[index];
    if (lastSong) {
        URL.revokeObjectURL(lastSong);
    }
    if (file) {
        lastSong = URL.createObjectURL(file);
        config.audio.file.src = lastSong;
        document.title = file.name;
    }
    changePlaylist(index);
}