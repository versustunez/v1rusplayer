Node.prototype.addDelegatedEventListener = function (type, aim, cb) {
    this.addEventListener(type, (event) => {
        let target = event.target;
        if (target.matches(aim)) {
            cb(event, target);
        } else {
            let parent = target.closest(aim);
            if (parent) {
                cb(event, parent);
            }
        }
    })
};